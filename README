Release notes for the DB XML API.

2017-02-10
Version 0.7
- Fixes/Changes
  + access point scanning via nmap to identify access points
  + user to access point (wagon) mapping
  + connected users count per access point (performed from script side, ap doesnt support this information)
  + wagon_status added with user count and UIC name (based on ap names)
  * Updated template and schema to match wagon_status and list
  + Upgraded to Python3
  + Reworked structure to make generators into modules.
  + Added external config "app_config"
  + ansible script for building app, based on app template 1.2
  Notes: Some data might not be available before AP scanning is complete, which at the first iteration will take up to 10 minutes. This data will later on be refresed every 10minutes.
  Notes: AP scanning scans every ap found in a set range and checks status with that AP, this is aggregated when the XML app displays the data. Meaning even if there are several APs in one wagon an aggregated status will be shown for that wagon.

2017-01-20
Version 0.6
- Fixes/Changes
  * aggregated rx/tx data is now sampled over an iterval and returns bit/s instead of total bits
  * zip_bandwidth now tries to contact db.custom-api for information about the current set available bandwidth.
  If no bandwidth is found the default, 300mbit (314572800bit) will be used
  * Script interval is lowered to 40s instead of 60s.
  * Updated NCC_Status_and_Gps.xsd

Version 0.5
- Fixes
  * wifi_status now returns correct data

Version 0.4
- Fixes
	* Added aggregated rx/tx with real data
	* Changed version number
	* Fixed an issue where UMTS technology would show a link as offline

Version 0.3
- Features
        * Lighttpd server that serves 3 API functions:
             - gps_and_status
             - class_status (params: userip)
             - portal_bandwidth
        * Python script for generating static XML files served by the lighttpd server.
        * Python script for collecting data from X6

The HTTP API serves static XML files generated every ~60 seconds over port 8081. Python scripts are run
every minute to update these XML files with fresh data. 
Calls to class_status containing an IP not currently in use will return dummy data.

Known issues:
      The following fields contain dummy data:
          * gps_and_status:
            - tzn
            - uic
            - status
            - connected_devices
          * class_status
            - uic
            - userclass

Refresh rate of XML files may be delayed.
Stability tests haven't been performed so stability is unknown.
