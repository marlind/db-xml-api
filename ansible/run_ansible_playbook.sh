#!/bin/bash -e

prog=$(basename $0)
dir=$(dirname $0)

usage() {
	echo "Usage: $prog <host> <playbook> [<ansible arg> ...]"
	exit 1
}

[ -z "$1" -o -z "$2" ] && usage
readonly HOST="$1"
readonly PLAYBOOK="$2"
shift; shift

ansible-playbook -i $dir/hosts -kKs --ask-vault-pass -l "${HOST}" -u root "$@" "${PLAYBOOK}"