<?php

$url = parse_url($_SERVER['REQUEST_URI']);
if ($url['path'] === '/gps_and_status') {
    header('Content-Type: application/xml');
    print file_get_contents('/mnt/media/gps_and_status.xml');
}
else if ($url['path'] === '/class_status') {
    if (isset($_GET['userip'])) {
        $ip = $_GET['userip'];
        $files = array_diff(scandir('/mnt/media/status'), ['.', '..']);
        $filename = "$ip.xml";
        // Conditionally whitelist which files to allow.
        if (in_array($filename, $files)) {
            header('Content-Type: application/xml');
            print file_get_contents("/mnt/media/status/$filename");
        }
        else {
            http_response_code(404);
            header('Content-Type: text/plain');
            print "No such user\n";
        }
    }
    else {
        header('Content-Type: application/xml');
        print file_get_contents("/mnt/media/status/all.xml");
    }
}
else if ($url['path'] === '/portal_bandwidth') {
    header('Content-Type: application/xml');
    print file_get_contents('/mnt/media/zip_bandwidth.xml');
}
else {
    http_response_code(404);
    header('Content-Type: text/plain');
    print "No such endpoint\n";
}
