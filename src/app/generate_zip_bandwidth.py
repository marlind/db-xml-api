import os
import time
import os.path
import requests
import json
import utils
import traceback
from lxml import etree

class GenerateZipBandwidth():
    def __init__(self, imp, config):
        self.imp = imp
        self.script_dir = os.path.dirname(os.path.abspath(__file__))
        self.jsonp_url = config["jsonp_url"]
        self.default_bandwidth = config["default_bandwidth"]
        self.api_url = config["api_url"]
        self.x_token = config["x_token"]

    def get_bandwidth_from_api(self, systemid, name, x_token):
        """Fetches assigned bandwidth for system name from db.custom-api"""

        req_data = {
            'action': 'bw',
            'systemid': systemid,
            'name': name,
            'x-token': x_token
        }
        try:
            r = requests.post(self.api_url, json=req_data)
            data = json.loads(r.text)
            bw = data.get('bandwidth', None)
            return bw
        except Exception as e:
            raise e


    def get_system_info(self):
        """Fetches system information from jsonp api"""

        try:
            r = requests.get(self.jsonp_url)
            data = json.loads(r.text[1:-3])
            return data.get("system_id", None), data.get("system_name", None)
        except Exception as e:
            raise e


    # Caches the bw data to limit the api calls
    def get_bandwidth(self):
        """Used to either fetch bandwidth from cache otherwise from db.custom-api"""

        bw = utils.get_from_cache("bw")
        if bw:
            return bw
        try:
            sysid, name = self.get_system_info()
            bw = self.get_bandwidth_from_api(sysid, name, self.x_token)
            if bw:
                utils.save_to_cache("bw", bw)
            return bw if bw else self.default_bandwidth
        except:
            print(traceback.format_exc())
            return self.default_bandwidth


    def gather_data_write_xml(self):
        print('Generating ZIP bandwidth...')
        tree = etree.parse(self.script_dir + '/templates/zip_bandwidth.xml')
        utils.insert_with_xpath(tree,
                                {'//zip_bandwidth_max': int(self.get_bandwidth())})
        utils.validate(tree, self.script_dir + '/schemas/NGC_Zip_Bandwidth.xsd')
        xml_path = '/mnt/media/zip_bandwidth.xml'
        tree.write(
            xml_path, encoding="UTF-8", xml_declaration=True, pretty_print=True)
        print('ZIP bandwidth saved to ' + xml_path)