import json
import os
import time
import os.path
import json
import subprocess
import traceback
from copy import deepcopy
from lxml import etree

def insert_with_xpath(root, values):
    """Inserts values into XML template, searches for root"""

    for query, value in list(values.items()):
        nodes = root.xpath(query)
        if len(nodes) == 1:
            node = nodes[0]
            if isinstance(value, list):
                for subvalues in value:
                    instance = deepcopy(node)
                    node.getparent().append(instance)
                    insert_with_xpath(instance, subvalues)
                node.getparent().remove(node)
            else:
                node.text = str(value)
        elif len(nodes) == 0:
            raise ValueError("XPath query returned no elements: " + query)
        else:
            raise ValueError("XPath query returned more than one element: " + query)

def validate(tree, schema):
    """Validates generated XML against set schema"""


    try:
        print('Validating against schema ' + schema)
        schema = etree.XMLSchema(etree.parse(schema))
        valid = schema.validate(tree)
        if valid:
            print('Validation successful!')
        else:
            print('Invalid XML generated:')
            for error in schema.error_log:
                print('- ' + str(error))
    except Exception as e:
        print(e)

def save_to_cache(identifier, data):
    with open("/tmp/" + identifier, 'w') as f:
        print("Saved data to cache")
        f.write(data)


def get_from_cache(identifier, timed=True):
    cache_time_minutes = 120
    try:
        file_path = "/tmp/" + identifier
        if os.path.isfile(file_path):
            with open(file_path, 'r') as f:
                st = os.stat(file_path)
                file_age = st.st_mtime
                curr_time = time.time()
                age = curr_time - file_age
                if not timed:
                    print("Fetched data from cache")
                    return f.read()
                elif age < cache_time_minutes * 60:
                    print("Fetched data from cache, time based")
                    return f.read()
    except:
        return None
    
def save_to_cache_as_json(identifier, data):
    json_data = json.dumps(data)
    save_to_cache(identifier, json_data)


def get_json_from_cache(identifier, timed=True):
    try:
        data = get_from_cache(identifier, timed)
        return json.loads(data)
    except:
        print("No JSON cache found")
        return None


def snmp(imp, command, *args, mib=True):
    """SNMP wrapper, pass in command and args, get get result back, mib param to support MIB requests"""

    directory = os.path.dirname(os.path.realpath(__file__))
    if mib:
        tokens = [
            command, '-M', directory + '/mibs', '-m', 'ALL', '-u', 'services',
            '-v', '3', '-l', 'authPriv', '-x', 'DES', '-X', 'bipdsosp!', '-a',
            'MD5', '-A', 'bipdsosp!', imp
        ] + list(args)
    else:
        tokens = [
            command, '-u', 'services',
            '-l', 'authPriv', '-x', 'DES', '-X', 'bipdsosp!', '-a',
            'MD5', '-A', 'bipdsosp!', imp
        ] + list(args)
    return subprocess.check_output(tokens).decode()
