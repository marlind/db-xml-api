#!/usr/bin/env bash

(
flock -x -n 200 || exit 1
echo "Scanning for APs"
/usr/bin/python3 /root/apscan/apscan_nmap.py > /dev/null 2>&1
) 200>/var/lock/apscan.lock