#!/usr/bin/env python3
import subprocess
import argparse
import datetime
import os
import sys
import traceback

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import utils

def discover_aps(scan_range):
    nmap = ['nmap', '-nsn', '--scan-delay', '0.5',  scan_range]

    proc = subprocess.Popen(nmap,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    out, err = proc.communicate()
    decoded_out = out.decode()
    lines = decoded_out.split('\n')
    ip_list = []
    for o in lines:
        if "Nmap scan report" in o:
            tokens = o.split()
            ip = tokens[-1]
            ip_list.append(ip)
    return ip_list


def snmp_get(ip, node):
    snmpget = ['snmpget', '-M', '/root/mibs', '-v', '2c', '-c', 'public']
    proc = subprocess.Popen(snmpget + [ip, node],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    out, err = proc.communicate()
    return out.decode()

def snmp_walk(ip):
    snmpwalk = ['snmpwalk', '-M', '/root/mibs', '-v', '2c', '-c', 'public']
    proc = subprocess.Popen(snmpwalk + [ip],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    out, err = proc.communicate()
    return out.decode()


def get_status_from_ap(ip):
    interfaces = {}
    apinfo = snmp_walk(ip)
    device_index_map = create_device_index_map(apinfo.split('\n'))

    # First get AP name
    out = snmp_get(ip, 'sysDescr.0')
    if 'Timeout' in out:
        result = {"AP": "Unreachable", "ip": ip, "online": "0", "interfaces": {}}
        return result
    # sysName.0 doesn't seem to have the name, so need to parse sysDescr.0
    # instead. Possible point of failure if the formatting changes.
    # First three tokens will always be ignorable at least.
    tokens = out.split()
    AP_name = tokens[4]
    result = {"ap": AP_name, "ip": ip, "online": "1"}

    for index in device_index_map:
        out = snmp_get(ip,"ifOperStatus." + str(index['index']))

        if 'up(1)' in out:
            interfaces[index['name']] = "1"
        else:
            interfaces[index['name']] = "0"

        result['interfaces'] = interfaces
    return result


def create_device_index_map(snmpwalk):
    result = []
    for line in snmpwalk:
        if "ifDescr" in line or "ifName" in line:
            tokens = line.split()
            if "wlan" in tokens[-1].lower() or "radio" in tokens[-1].lower():
                index = int(tokens[0].split(".")[-1])
                index_dict = {'name': tokens[-1].lower(),
                              'index': index}
                result.append(index_dict)

    return result


def find_ip_range(scan_range):
    nmap = ['nmap', '-nsn', '--scan-delay', '0.5',  scan_range]

    proc = subprocess.Popen(nmap,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    out, err = proc.communicate()
    decoded_out = out.decode()
    lines = decoded_out.split('\n')
    for o in lines:
        if "Nmap scan report" in o:
            tokens = o.split()
            ip = tokens[-1]
            ip_tokens = ip.split(".")
            ip_range = ip_tokens[0] + "." + ip_tokens[1] + "." + ip_tokens[2] + ".0/24"
            with open("/tmp/ip_range", 'w') as f:
                f.write(ip_range)
            return ip_range
    return None


def get_ap_list():
    ips = None
    scan_range = "10.101.2-30.1-254"
    if not os.path.isfile("/tmp/ip_range"):
        print("No ip range cache")
        ip_range = find_ip_range(scan_range)
        if not ip_range:
            raise
        ips = discover_aps(ip_range)
    else:
        with open("/tmp/ip_range", 'r') as f:
            print("IP range cache found")
            ip_range = f.readline()
            ips = discover_aps(ip_range)
    return ips


def get_ap_status(ips):
    payload = []
    for ip in ips:
        r = get_status_from_ap(ip)
        payload.append(r)
    return payload


def save_last_run():
    with open("/tmp/apscan_last_run", "w") as f:
        f.write('%s' % datetime.datetime.now())


def generate_ap_status_data():
    try:
        print("Searching for APS")
        ips = get_ap_list()
        print("Fetching AP statuses")
        aps = get_ap_status(ips)
        print("Saving to cache")
        utils.save_to_cache_as_json('aps', aps)
        print("Saving last run")
        save_last_run()
    except:
        print("Could not find AP IP range, exiting")
        print((traceback.format_exc()))
        sys.exit(0)


if __name__=='__main__':
    generate_ap_status_data()
