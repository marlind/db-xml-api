import os.path
import os
import time
import utils
from lxml import etree


class GenerateUserClass():
    def __init__(self, imp, config):
        self.script_dir = os.path.dirname(os.path.abspath(__file__))
        self.imp = imp
        self.config = config

    def user_mapping(self, user):
        """Prapares user data for XML"""

        uic = user['uic']
        return {
            'ip': user['ip'],
            'uic': uic,
            'timestamp': int(user['timestamp']),
            'rxOctets': user['rx'],
            'txOctets': user['tx'],
            'mac': user['mac'],
            'userclass': user['class'],
            'state': 'INTERNET'
            if user['state'] == 'AUTHORIZED' else 'NON_INTERNET'
        }

    def save_user_ap_list(self, ap_users):
        """Saves ap list to cache"""

        utils.save_to_cache_as_json("ap_users", ap_users)

    def find_wagon_for_ip(self, ip, data):
        """Locates and parses wagon UIC from AP name"""

        for ap in data:
            if ap["ip"] == ip:
                return ap["ap"][5:-2]

    def get_user_uic(self, ip):
        """Fetches AP name for user"""

        try:
            data = utils.get_json_from_cache("aps", timed=False)
            if data:
                ap = self.find_wagon_for_ip(ip, data)
                if ap:
                    return ap
            return "No UIC"
        except:
            return "No UIC"

    def gather_user_data(self, imp):
        """Fetches all connected users
        Saves ip, mac, state, rx, tx, class, ap ip
        Fetches UIC name from AP ip
        returns list with user dicts
        """

        users_list = []
        index_list = []
        ap_users = {}

        out = utils.snmp(self.imp, "snmpwalk", ".1.3.6.1.4.1.10581.2.5.10.1.1", mib=False)
        lines = out.split('\n')
        if not lines:
            return users_list
        for l in lines:
            if l:
                tokens = l.split()
                index = str(tokens[0].split(".")[-1])
                index_list.append(index)

        out = utils.snmp(self.imp, "snmpwalk", ".1.3.6.1.4.1.10581.2.5.10", mib=False)
        data = out.split('\n')
        for i in index_list:
            user_dict = {
                "ip": "",
                "mac": "",
                "state": "",
                "timestamp": time.time(),
                "rx": "",
                "tx": "",
                "uic": "12345",
                "class": "2",
                "ap_ip": "",
                "uic": ""
            }
            for d in data:
                if d:
                    d_toks = d.split()
                    OID_toks = d_toks[0].split(".")
                    if i == OID_toks[-1]:
                        if OID_toks[-2] == '2':
                            user_dict["ip"] = d_toks[-1].strip('"')
                        elif OID_toks[-2] == '3':
                            user_dict["mac"] = d_toks[-1].strip('"')
                        elif OID_toks[-2] == '4':
                            user_dict["state"] = d_toks[-1].strip('"')
                        elif OID_toks[-2] == '7':
                            user_dict["rx"] = d_toks[-1]
                        elif OID_toks[-2] == '8':
                            user_dict["tx"] = d_toks[-1]
                        elif OID_toks[-2] == '9':
                            user_dict["class"] = d_toks[-1]
                        elif OID_toks[-2] == '10':
                            ap = d_toks[-1].replace('\"', '')
                            user_dict["ap_ip"] = ap if ap != "-1" else "local"
            user_dict["uic"] = self.get_user_uic(user_dict["ap_ip"])
            ap_users.setdefault(user_dict["ap_ip"], []).append(user_dict["ip"])
            users_list.append(user_dict)
        self.save_user_ap_list(ap_users)
        return users_list

    def write_xml(self, path, ips):
        """Generates XML"""
        
        template_path = self.script_dir + '/templates/user_class.xml'
        tree = etree.parse(template_path)
        utils.insert_with_xpath(
            tree, {'//class_status': [self.user_mapping(ip) for ip in ips]})
        utils.validate(tree, self.script_dir + '/schemas/NGC_Class_Status.xsd')
        tree.write(
            path, encoding='UTF-8', xml_declaration=True, pretty_print=True)

    def gather_data_write_xml(self):
        """Called upton to fetch data and write xml"""
        
        # check if status path exist
        status_path = self.config["user_path"]
        if not os.path.isdir(status_path) and not os.path.isfile(status_path):
            os.makedirs(status_path)

        users = self.gather_user_data(self.imp)
        self.write_xml(self.config["all_path"], users)
        for user in users:
            user_path = self.config["user_path"] + str(user['ip']) + '.xml'
            self.write_xml(user_path, [user])
