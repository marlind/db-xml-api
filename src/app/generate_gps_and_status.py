import json
import os
import os.path
import requests
import subprocess
import time
import utils
from copy import deepcopy
from lxml import etree


class GenerateGpsAndStatus():
    def __init__(self, imp, config):
        """
        Class for generating 
        config should be passed as dict
        """

        self.script_dir = os.path.dirname(os.path.abspath(__file__))
        self.imp = imp
        self.config = config
        self.json_api_host = config["json_api_host"]

    # PARSERS

    def link_status(self, link):
        """Converts link state to network technology. 2G, 3G, 3,5G, 4G"""

        if link['device_state'] != 'up' or link['link_state'] != 'available':
            return 'offline'
        elif 'device_subtype' in link and link[
                'device_subtype'] == 'unavailable':
            return "deactivated"
        elif 'technology' in link:
            if link['technology'] == 'lte':
                return '4G'
            elif 'dc-hspa+' in link['technology']:
                return '3.5G'
            elif ('hsdpa' in link['technology'] or
                  'cdma' in link['technology'] or
                  'hspa+' in link['technology'] or
                  'umts' in link['technology'] or
                  'hspa' in link['technology']):
                return '3G'
            elif ('gprs' in link['technology'] or
                  'edge' in link['technology']):
                return '2G'
            else:
                return 'offline'
        else:
            return 'offline'

    def connection_score(self, links):
        """Aggregates connection score per technology"""

        score = 0
        for l in links:
            score += {'4G': 2, '3.5G': 1, '3G': 1}.get(self.link_status(l), 0)
        score = min(10, score)
        return score

    def modem_mapping(self, link):
        """Prepares link data for XML"""

        return {
            'modem_id': link['index'],
            'modem_status': self.link_status(link),
            'rx_used': int(link['rx']),
            'tx_used': int(link['tx']),
            'imsi': link['imsi'],
            'mcc': link['mcc'],
            'mnc': link['mnc'],
        }

    def wagon_status_mapping(self, wagon_status_list):
        """Prepares wagon status for XML"""

        return {
            'uic': wagon_status_list['uic'],
            'wifi_status': wagon_status_list['wifi_status'],
            'connected_devices': wagon_status_list['connected_devices']
        }

    def generate_wagon_status(self, ap_status_list):
        """Add all access point to the correct wagon, will create arrays with aggregated results"""

        wagon_array = []
        wagon_dict = dict()
        for ap in ap_status_list:
            wagon_status = {"wifi_status": [], "connected_devices": []}
            wagon_name = ap["ap_name"][:-2]

            curr_wagon_dict = wagon_dict.setdefault(wagon_name, wagon_status)
            curr_wagon_dict["wifi_status"].append(ap["wifi_status"])
            curr_wagon_dict["connected_devices"].append(ap[
                "connected_devices"])

        # Sumarize content, connected devices, etc
        for wagon, status in wagon_dict.items():
            wagon_status = {
                "uic": wagon,
                "wifi_status": "on" if "1" in status["wifi_status"] else "off",
                "connected_devices": sum(status["connected_devices"])
            }
            wagon_array.append(wagon_status)
        return wagon_array

    # DATA FETCHING

    def gather_gps_data(self):
        """Fetches current GPS data from X6 jsonp API"""

        r = requests.get(self.json_api_host + "position/")
        gps_json = r.text.strip().strip("(); ")
        gps_dict = json.loads(gps_json)

        for key in ['time', 'age', 'satellites', 'mode']:
            gps_dict[key] = int(gps_dict[key])

        for key in ['latitude', 'longitude', 'altitude', 'speed', 'cmg']:
            gps_dict[key] = float(gps_dict[key])

        return gps_dict

    def get_tzn(self):
        """Fetches train TZN number (X6 name)"""

        output = utils.snmp(self.imp, 'snmpget', 'icomeraImpSystemDeviceName')
        host = output.split()[-1]
        tzn = host.upper()
        return tzn

    def gather_connectivity_data(self):
        """Fetches information about modem information
            * Current model status
            * Samples and aggregates RX/TX for each modem
            * Aggregates RX/TX for all modems
            * IMSI, MCC, MNC for modems/sims
        """
        aggregated_rx = 0
        aggregated_tx = 0
        link_data_dict = {}
        sample_interval = 2
        sample_sleep_time = 10

        r = requests.get(self.json_api_host + "connectivity/")
        con_json = r.text.strip().strip("(); ")
        con_dict = json.loads(con_json)

        # Set amount of samples, iterate X times, get tx delta, devide over iteration
        for i in range(0, sample_interval):
            for l in con_dict['links']:
                l['rx'] = 0
                l['tx'] = 0
                l['imsi'] = 0
                l['mcc'] = 0
                l['mnc'] = 0

                id = "ppp" + l['index']

                out = utils.snmp(self.imp, "snmpwalk", "ifDescr")
                tokens = out.split('\n')
                line = ""

                for t in tokens:
                    if id in t.lower():
                        line = t
                if not line:
                    continue

                tokens = line.split()
                if id in tokens[-1].lower():
                    index = str(tokens[0].split(".")[-1])

                out = utils.snmp(self.imp, "snmpget", "ifOutOctets." + index)
                l['tx'] = int(out.split()[-1])

                out = utils.snmp(self.imp, "snmpget", "ifInOctets." + index)
                l['rx'] = int(out.split()[-1])

                if i == 0:
                    # Reset modem total first iteration
                    l['aggregated_modem_rx'] = 0
                    l['aggregated_modem_tx'] = 0

                    # Base to calculate delta
                    l['sample_base_rx'] = l['rx']
                    l['sample_base_tx'] = l['tx']

                # Aggregation and bit/s need to be both for total and per modem
                aggregated_rx += (l['rx'] - l['sample_base_rx'])
                aggregated_tx += (l['tx'] - l['sample_base_tx'])
                l['aggregated_modem_rx'] += (l['rx'] - l['sample_base_rx'])
                l['aggregated_modem_tx'] += (l['tx'] - l['sample_base_tx'])

                # Save bit/s per modem after last iteration
                if i == (sample_interval - 1):
                    l['rx'] = int(l['aggregated_modem_rx'] / sample_interval)
                    l['tx'] = int(l['aggregated_modem_tx'] / sample_interval)

                # IMSI
                out = utils.snmp(
                    self.imp, "snmpget",
                    ".1.3.6.1.4.1.10581.2.3.11.1.3." + str(l['index']))
                l['imsi'] = out.split()[-1].strip('"')
                l['mcc'] = l['imsi'][:3]
                l['mnc'] = l['imsi'][3:5]
            if i == 0:
                time.sleep(sample_sleep_time)

        # Saving total delta devided by sample interval, will give us estimated avg bit/s during the measurements
        con_dict['aggregated_tx'] = int(aggregated_tx / sample_interval)
        con_dict['aggregated_rx'] = int(aggregated_rx / sample_interval)
        return con_dict

    def get_connected_devices_for_ip(self, ip):
        """Fetches users connected to AP, generated via generate_user_class"""
        try:
            ap_users = utils.get_json_from_cache("ap_users", timed=False)
            if not ap_users:
                return 0
            else:
                return len(ap_users[ip])
        except:
            return 0

    def gather_ap_data(self):
        """Fetches AP list from cache, nmap script generates this"""
        data = utils.get_json_from_cache('aps', timed=False)
        aps = []
        if not data:
            return []
        else:
            for ap in data:
                ap_name = ap['ap'][5:]
                ap_name = ap_name if ap_name != "-1" else "local"
                ap_dict = {
                    'ap_name': ap_name,
                    'ip': ap["ip"],
                    'wifi_status': ap['online'],
                    'connected_devices':
                    self.get_connected_devices_for_ip(ap["ip"])
                }
                aps.append(ap_dict)
            return aps

    # OUTPUT

    def write_xml(self, gps_data, tzn_data, connectivity_data,
                  wagon_status_data):
        """ Writes gps and status to XML"""
        tree = etree.parse(self.script_dir + '/templates/gps_and_status.xml')
        wagon_statuses = [
            self.wagon_status_mapping(wagon) for wagon in wagon_status_data
            if wagon['uic'] != 'local'
        ]
        modem_list = [
            self.modem_mapping(link) for link in connectivity_data['links']
        ]

        utils.insert_with_xpath(tree, {
            '//tzn': tzn_data,
            '//train/uic': 'no train UIC',
            '//satelites': max(0, gps_data['satellites']),
            '//latitude': gps_data['latitude'],
            '//longitude': gps_data['longitude'],
            '//direction': gps_data['cmg'],
            '//speed': gps_data['speed'],
            '//connection/status':
            self.connection_score(connectivity_data['links']),
            '//aggregated_rx_used': connectivity_data['aggregated_rx'],
            '//aggregated_tx_used': connectivity_data['aggregated_tx'],
            '//wagon_status': wagon_statuses,
            '//modem': modem_list
        })

        utils.validate(tree,
                       self.script_dir + '/schemas/NGC_Status_and_Gps.xsd')
        xml_path = '/mnt/media/gps_and_status.xml'
        tree.write(
            xml_path,
            encoding="UTF-8",
            xml_declaration=True,
            pretty_print=True)

    def gather_data_write_xml(self):
        """Fetches all data and calls write xml"""

        gps_data = self.gather_gps_data()
        tzn_data = self.get_tzn()
        connectivity_data = self.gather_connectivity_data()
        access_point_data = self.gather_ap_data()
        wagon_status_data = self.generate_wagon_status(access_point_data)
        self.write_xml(gps_data, tzn_data, connectivity_data,
                       wagon_status_data)
