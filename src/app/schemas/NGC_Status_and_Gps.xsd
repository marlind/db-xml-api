<?xml version="1.0" encoding="UTF-8" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

<xs:complexType name="gpsType">
    <xs:annotation>
        <xs:documentation xml:lang="en">
            This type contains the GPS data.
        </xs:documentation>
    </xs:annotation>
    <xs:all>
        <xs:element name="satelites">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    number of visible satelites
                </xs:documentation>
            </xs:annotation>
            <xs:simpleType>
                <xs:restriction base="xs:integer">
                    <xs:minInclusive value="0"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:element>
        <xs:element name="latitude" type="xs:float" />
        <xs:element name="longitude" type="xs:float" />
        <xs:element name="direction" type="xs:float" />
        <xs:element name="speed" type="xs:float" />
    </xs:all>
</xs:complexType>

<xs:complexType name="trainType">
    <xs:annotation>
        <xs:documentation xml:lang="en">
            This type contains information about the train with wagon
        </xs:documentation>
    </xs:annotation>
    <xs:all>
        <xs:element name="tzn" type="xs:string">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    tz number of the train
                    (may not always be up to date with the information
                    of the trains router)
                </xs:documentation>
            </xs:annotation>
        </xs:element>
        <xs:element name="uic" type="xs:string">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    uic number of the wagon the main router is installed in
                    (may not always be up to date with the information
                    of the trains router)
                </xs:documentation>
            </xs:annotation>
        </xs:element>
    </xs:all>
</xs:complexType>

<xs:complexType name="connectionType">
    <xs:annotation>
        <xs:documentation xml:lang="en">
            This type contains information about the train with wagon
        </xs:documentation>
    </xs:annotation>
    <xs:all>
        <xs:element name="status">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    status of the train to land connection
                    0 = deactivated, 1-10 = very bad to very good
                </xs:documentation>
            </xs:annotation>
            <xs:simpleType>
                <xs:restriction base="xs:integer">
                    <xs:minInclusive value="0"/>
                    <xs:maxInclusive value="10"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:element>
        <xs:element name="aggregated_rx_used" type="xs:integer" minOccurs="0">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    aggregated recieved bandwidth in bit/s
                </xs:documentation>
            </xs:annotation>
        </xs:element>
        <xs:element name="aggregated_tx_used" type="xs:integer" minOccurs="0">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    aggregated transmitted bandwidth in bit/s
                </xs:documentation>
            </xs:annotation>
        </xs:element>
        <xs:element name="modem_list">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    a list of the installed modems
                </xs:documentation>
            </xs:annotation>
            <xs:complexType>
                <xs:sequence minOccurs="0" maxOccurs="unbounded">
                    <xs:element name="modem" type="modemType">
                    </xs:element>
                </xs:sequence>
            </xs:complexType>
        </xs:element>
    </xs:all>
</xs:complexType>

<xs:complexType name="modemType">
    <xs:annotation>
        <xs:documentation xml:lang="en">
            this type represents a modem
        </xs:documentation>
    </xs:annotation>
    <xs:all>
        <xs:element name="modem_id">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    id of the modem (between 101 and 106)
                </xs:documentation>
            </xs:annotation>
            <xs:simpleType>
                <xs:restriction base="xs:integer">
                    <xs:minInclusive value="101"/>
                    <xs:maxInclusive value="106"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:element>
        <xs:element name="modem_status">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    the status of the modem
                </xs:documentation>
            </xs:annotation>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="2G" />
                    <xs:enumeration value="3G" />
                    <xs:enumeration value="3.5G" />
                    <xs:enumeration value="4G" />
                    <xs:enumeration value="5G" />
                    <xs:enumeration value="offline" />
                    <xs:enumeration value="deactivated" />
                </xs:restriction>
            </xs:simpleType>
        </xs:element>
        <xs:element name="rx_used" type="xs:integer">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    current recieving bandwidth in bit/s
                </xs:documentation>
            </xs:annotation>
        </xs:element>
        <xs:element name="tx_used" type="xs:integer">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    current transmitting bandwidth in bit/s
                </xs:documentation>
            </xs:annotation>
        </xs:element>
        <xs:element name="imsi" type="xs:string">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    IMSI of the SIM card this modem is currently associated with
                </xs:documentation>
            </xs:annotation>
        </xs:element>
        <xs:element name="mcc" type="xs:string">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    mmc of the mobile network used, if online
                </xs:documentation>
            </xs:annotation>
        </xs:element>
        <xs:element name="mnc" type="xs:string">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    mnc of the mobile network used, if online
                </xs:documentation>
            </xs:annotation>
        </xs:element>
    </xs:all>
</xs:complexType>

<xs:complexType name="wagonStatusType">
    <xs:annotation>
        <xs:documentation xml:lang="en">
            This type contains information about the train with wagon
        </xs:documentation>
    </xs:annotation>
    <xs:all>
        <xs:element name="uic">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    the uic wagon number of this wagon
                </xs:documentation>
            </xs:annotation>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:minLength value="1"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:element>
        <xs:element name="wifi_status">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    the wifi status of this wagon
                </xs:documentation>
            </xs:annotation>
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="on" />
                    <xs:enumeration value="off" />
                </xs:restriction>
            </xs:simpleType>
        </xs:element>
        <xs:element name="connected_devices" minOccurs="0">
            <xs:annotation>
                <xs:documentation xml:lang="en">
                    number of devices connected to all AP-modules in this wagon
                </xs:documentation>
            </xs:annotation>
            <xs:simpleType>
                <xs:restriction base="xs:integer">
                    <xs:minInclusive value="0"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:element>
    </xs:all>
</xs:complexType>


<xs:element name="gps_and_status">
    <xs:annotation>
        <xs:documentation xml:lang="en">
            The status of the NGC system and gps information.
        </xs:documentation>
    </xs:annotation>
    <xs:complexType>
        <xs:all>
            <xs:element name="version" type="xs:decimal">
                <xs:annotation>
                    <xs:documentation xml:lang="en">
                        api version
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="train" type="trainType" minOccurs="0">
                <xs:annotation>
                    <xs:documentation xml:lang="en">
                        information about the train and wagon
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="geo_information" type="gpsType" minOccurs="0">
                <xs:annotation>
                    <xs:documentation xml:lang="en">
                         position of the train as gps coordinates including
                         heading and speed
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="connection" type="connectionType">
                <xs:annotation>
                    <xs:documentation xml:lang="en">
                         status of the train-land-connection
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="wagon_list">
                <xs:annotation>
                    <xs:documentation xml:lang="en">
                        a list of wagon statuses
                    </xs:documentation>
                </xs:annotation>
                <xs:complexType>
                    <xs:sequence minOccurs="0" maxOccurs="unbounded">
                        <xs:element name="wagon_status" type="wagonStatusType">
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:all>
    </xs:complexType>
</xs:element>

</xs:schema>
