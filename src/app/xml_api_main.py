#!/usr/bin/env python3
from generate_gps_and_status import GenerateGpsAndStatus
from generate_user_class import GenerateUserClass
from generate_zip_bandwidth import GenerateZipBandwidth
import json
import time
import sys
import os.path
import datetime
from app_config import configuration as cfg

def save_last_run():
    with open("/tmp/xmlapi_last_run", "w") as f:
        f.write('%s' % datetime.datetime.now())

if __name__ == "__main__":
    imp_adr = cfg["general"]["imp_adr"]
    period = cfg["general"]["period"]

    gps_and_status = GenerateGpsAndStatus(imp_adr, cfg["gps_and_status"])
    user_class = GenerateUserClass(imp_adr, cfg["user_class"])
    zip_bandwidth = GenerateZipBandwidth(imp_adr, cfg["zip_bandwidth"])

    while True:
        try:
            print("Generating user data")
            user_class.gather_data_write_xml()
            print("Generating gps and status data")
            gps_and_status.gather_data_write_xml()
            print("Generating bandwidth data")
            zip_bandwidth.gather_data_write_xml()
            print('Sleeping for ' + str(period) + '...')
            sys.stdout.flush()  # Debug
            save_last_run()
            time.sleep(period)
        except KeyboardInterrupt:
            sys.exit()
